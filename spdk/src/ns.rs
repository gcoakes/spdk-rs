use std::{ffi::c_void, marker::PhantomData, os::raw::c_int};

use spdk_sys::*;

use crate::{
    util::{cmd_cb, DropBox},
    Buffer, Error, Qpair, Result,
};

#[derive(Debug, Hash, PartialEq, Eq)]
pub struct Namespace {
    ptr: *mut spdk_nvme_ns,
    phantom: PhantomData<spdk_nvme_ns>,
}

/// Safety: The public safe API which uses `Namespace` has been carefully
/// crafted to protect the invariants described by the upstream documentation.
unsafe impl Send for Namespace {}
unsafe impl Sync for Namespace {}

impl Namespace {
    pub unsafe fn steal_ptr(ns: *mut spdk_nvme_ns) -> Self {
        Self {
            ptr: ns,
            phantom: PhantomData,
        }
    }

    pub fn ns_data(&self) -> &nvme::IdNmsp {
        // Safety: Documentation indicates `spdk_nvme_ns_get_data` is thread
        // safe and may be called as long as the namespace is attached.
        unsafe { &*spdk_nvme_ns_get_data(self.ptr).cast() }
    }

    pub fn nsid(&self) -> u32 {
        // Safety: Documentation indicates `spdk_nvme_ns_get_id` is thread
        // safe and may be called as long as the namespace is attached.
        unsafe { spdk_nvme_ns_get_id(self.ptr) }
    }

    pub fn size(&self) -> u64 {
        // Safety: Documentation indicates `spdk_nvme_ns_get_size` is thread
        // safe and may be called as long as the namespace is attached.
        unsafe { spdk_nvme_ns_get_size(self.ptr) }
    }

    pub fn read<F>(
        &self,
        qpair: &Qpair,
        payload: &mut Buffer,
        lba: u64,
        lba_count: u32,
        flags: u32,
        callback: F,
    ) -> Result<()>
    where
        F: FnOnce(&nvme::CompletionQueueEntry) + 'static,
    {
        self.io(
            qpair,
            payload,
            lba,
            lba_count,
            flags,
            callback,
            spdk_nvme_ns_cmd_read,
        )
    }

    pub fn write<F>(
        &self,
        qpair: &Qpair,
        payload: &mut Buffer,
        lba: u64,
        lba_count: u32,
        flags: u32,
        callback: F,
    ) -> Result<()>
    where
        F: FnOnce(&nvme::CompletionQueueEntry) + 'static,
    {
        self.io(
            qpair,
            payload,
            lba,
            lba_count,
            flags,
            callback,
            spdk_nvme_ns_cmd_write,
        )
    }

    #[inline]
    fn io<F>(
        &self,
        qpair: &Qpair,
        payload: &mut Buffer,
        lba: u64,
        lba_count: u32,
        flags: u32,
        callback: F,
        rw: unsafe extern "C" fn(
            *mut spdk_nvme_ns,
            *mut spdk_nvme_qpair,
            *mut c_void,
            u64,
            u32,
            spdk_nvme_cmd_cb,
            *mut c_void,
            u32,
        ) -> c_int,
    ) -> Result<()>
    where
        F: FnOnce(&nvme::CompletionQueueEntry) + 'static,
    {
        // This function is generic over F and doesn't use dyn F, so no need for
        // double indirection à la
        // https://stackoverflow.com/questions/32270030/how-do-i-convert-a-rust-closure-to-a-c-style-callback.
        let ctx = Box::new(callback);
        let res = unsafe {
            rw(
                self.ptr,
                qpair.ptr,
                payload.ptr,
                lba,
                lba_count,
                Some(cmd_cb::<F>),
                // Leaks the box which must be re-aquired by the callback.
                Box::into_raw(ctx).cast(),
                flags,
            )
        };
        match -res {
            errno::EINVAL => Err(Error::CmdMalformed),
            errno::ENOMEM => Err(Error::MemoryAllocation),
            errno::ENXIO => Err(Error::Transport),
            // This handles both 0 and -EFAULT. -EFAULT must be handled on
            // the completion entry side because the documentation indicates
            // it will still call the callback function.
            _ => Ok(()),
        }
    }

    pub fn awrite(
        &self,
        qpair: &Qpair,
        payload: &mut Buffer,
        lba: u64,
        lba_count: u32,
        flags: u32,
    ) -> DropBox<Result<nvme::CompletionQueueEntry>> {
        let dropbox = DropBox::new();
        let (waker, dest) = dropbox.waker_pair();
        let res = self.write(qpair, payload, lba, lba_count, flags, move |cpl| {
            if let Some(dest) = dest.upgrade() {
                dest.replace(Some(Ok(*cpl)));
            }
            if let Some(w) = waker.upgrade().and_then(|w| w.replace(None)) {
                w.wake()
            }
        });
        if let Err(err) = res {
            dropbox.dest.replace(Some(Err(err)));
        }
        dropbox
    }

    pub fn aread(
        &self,
        qpair: &Qpair,
        payload: &mut Buffer,
        lba: u64,
        lba_count: u32,
        flags: u32,
    ) -> DropBox<Result<nvme::CompletionQueueEntry>> {
        let dropbox = DropBox::new();
        let (waker, dest) = dropbox.waker_pair();
        let res = self.read(qpair, payload, lba, lba_count, flags, move |cpl| {
            if let Some(dest) = dest.upgrade() {
                dest.replace(Some(Ok(*cpl)));
            }
            if let Some(w) = waker.upgrade().and_then(|w| w.replace(None)) {
                w.wake()
            }
        });
        if let Err(err) = res {
            dropbox.dest.replace(Some(Err(err)));
        }
        dropbox
    }
}
