use std::mem::MaybeUninit;

use spdk_sys::*;

use crate::{Error, Result};

#[derive(Default)]
pub struct Env {
    /// The shared memory ID of the environment.
    pub shm_id: i32,
    /// Must be a null terminated statically allocated string. i.e.:
    /// `b"spdk-rs\0"`
    pub name: &'static [u8],
}

impl Env {
    pub fn new() -> Env {
        Env {
            name: b"spdk-rs\0",
            ..Default::default()
        }
    }

    pub fn shm_id(&mut self, shm_id: i32) -> &mut Self {
        self.shm_id = shm_id;
        self
    }

    pub fn name(&mut self, name: &'static [u8]) -> &mut Self {
        self.name = name;
        self
    }

    pub fn init(&mut self) -> Result<()> {
        // Safety: `spdk_env_opts_init` guarantees `new_opts` is initialized
        // before usage.
        let mut opts = unsafe {
            let mut new_opts = MaybeUninit::uninit();
            spdk_env_opts_init(new_opts.as_mut_ptr());
            new_opts.assume_init()
        };
        opts.name = self.name.as_ptr() as *const i8;
        opts.shm_id = self.shm_id;
        // Safety: The `opts` outlives the pointer sent to the FFI call.
        let res = unsafe { spdk_env_init(&opts as *const _) };
        if res != 0 {
            Err(Error::EnvInit(res))
        } else {
            Ok(())
        }
    }
}
