use core::slice;
use std::{
    ffi::c_void,
    ops::{Index, IndexMut},
    ptr,
    slice::SliceIndex,
};

use spdk_sys::*;

#[derive(Debug)]
pub struct Buffer {
    pub ptr: *mut c_void,
    pub size: usize,
    pub kind: BufferKind,
}

#[derive(Debug)]
pub enum BufferKind {
    // TODO: How to express this properly? It appears as if controller mapped
    // memory is global to the device, since `spdk_nvme_ctrlr_unmap_cmd` only
    // takes a pointer to the controller. How does it associate with the
    // specific buffer that was allocated?
    // CtrlrMapped { ctrlr: &'a SpdkCtrlr },
    DirectMemory,
}

/// Safety: Buffer is essentially a `&mut [u8]` but for shared memory. Following
/// the same logic as `&mut T`'s implementation of `Send`, `Buffer` can implement
/// it.
unsafe impl Send for Buffer {}

impl Drop for Buffer {
    fn drop(&mut self) {
        match self.kind {
            // Safety: `self.ptr` is allocated by and owned by `self`. It can be
            // freed when dropped.
            BufferKind::DirectMemory => unsafe {
                spdk_free(self.ptr);
            },
        }
    }
}

impl Buffer {
    // TODO: Is this thread safe? Should ctrlr be & or &mut?
    pub fn on_ctrlr(_ctrlr: &crate::Controller, desired_size: size_t, alignment: size_t) -> Self {
        // unsafe {
        //     let mut size = MaybeUninit::uninit();
        //     let buf = spdk_nvme_ctrlr_map_cmb(ctrlr.attached_ptr(), size.as_mut_ptr());
        //     let size = size.assume_init();
        //     if buf.is_null() || size < desired_size {
        //         return SpdkBuffer::CtrlrMapped {
        //             ptr: buf,
        //             size: size,
        //         };
        //     }
        // }
        Self::new(desired_size, alignment)
    }

    pub fn new(size: size_t, alignment: size_t) -> Self {
        Buffer {
            // Safety: This is just malloc for SPDK.
            ptr: unsafe {
                spdk_zmalloc(
                    size,
                    alignment,
                    ptr::null_mut(),
                    SPDK_ENV_SOCKET_ID_ANY,
                    SPDK_MALLOC_DMA,
                )
            },
            size: size as usize,
            kind: BufferKind::DirectMemory,
        }
    }

    pub fn ptr(&mut self) -> *mut c_void {
        self.ptr
    }

    pub fn len(&self) -> usize {
        self.size
    }
}

impl<I: SliceIndex<[u8]>> Index<I> for Buffer {
    type Output = I::Output;

    fn index(&self, index: I) -> &Self::Output {
        Index::index(
            // Safety: `self.ptr` points to memory that has been preivously
            // validated and is guaranteed (by SPDK's documentation) to be of
            // `self.size` length. Further, a pointer to `u8` should be valid to
            // reference for any location because the alignment of `u8` is 1.
            // Finally, `self.ptr` is only ever stored within `Buffer`, so
            // normal mutability guarantees should protect the memory from
            // mutation.
            unsafe { slice::from_raw_parts(self.ptr as *const _, self.size) },
            index,
        )
    }
}

impl<I: SliceIndex<[u8]>> IndexMut<I> for Buffer {
    fn index_mut(&mut self, index: I) -> &mut Self::Output {
        IndexMut::index_mut(
            // Safety: `self.ptr` points to memory that has been preivously
            // validated and is guaranteed (by SPDK's documentation) to be of
            // `self.size` length. Further, a pointer to `u8` should be valid to
            // reference for any location because the alignment of `u8` is 1.
            // Finally, `self.ptr` is only ever stored within `Buffer`, so
            // normal mutability guarantees should protect the memory from
            // mutation.
            unsafe { slice::from_raw_parts_mut(self.ptr as *mut _, self.size) },
            index,
        )
    }
}

impl AsRef<[u8]> for Buffer {
    fn as_ref(&self) -> &[u8] {
        &self[..]
    }
}

impl AsMut<[u8]> for Buffer {
    fn as_mut(&mut self) -> &mut [u8] {
        &mut self[..]
    }
}
