/*! Rustic bindings to spdk-sys crate. */

use std::{
    collections::HashMap,
    ffi::c_void,
    mem::MaybeUninit,
    ptr,
    sync::atomic::{AtomicBool, Ordering},
    sync::Arc,
};

use async_lock::{RwLock, RwLockWriteGuard};
use futures_lite::future::yield_now;

use lazy_static::lazy_static;
use spdk_sys::*;

mod buffer;
#[doc(inline)]
pub use buffer::*;

mod ctrlr;
#[doc(inline)]
pub use ctrlr::*;

mod env;
#[doc(inline)]
pub use env::*;

mod error;
#[doc(inline)]
pub use error::*;

mod ns;
#[doc(inline)]
pub use ns::*;

mod qpair;
#[doc(inline)]
pub use qpair::*;

mod util;

lazy_static! {
    static ref REMOVED_REFS: Arc<RwLock<HashMap<usize, Arc<AtomicBool>>>> =
        Arc::new(RwLock::new(HashMap::new()));
}

/// Probe the system for viable devices to which the SPDK driver should attach.
///
/// * `on_probe` - Decide if the given transport ID (i.e.: PCI address) should
///   be attached to the driver.
/// * `on_attach` - Notification of a device which was attached.
///
/// If any device is determined to have been removed since the last probe call,
/// then `spdk::Controller::removed` will be updated to reflect the change.
async fn probe<P, A>(mut on_probe: P, mut on_attach: A) -> Result<()>
where
    P: FnMut(&spdk_nvme_transport_id, &mut spdk_nvme_ctrlr_opts) -> bool,
    A: FnMut(&spdk_nvme_transport_id, Controller, &spdk_nvme_ctrlr_opts),
{
    struct ProbeCtx<'a> {
        on_probe: &'a mut dyn FnMut(&spdk_nvme_transport_id, &mut spdk_nvme_ctrlr_opts) -> bool,
        on_attach: &'a mut dyn FnMut(&spdk_nvme_transport_id, Controller, &spdk_nvme_ctrlr_opts),
        devs: RwLockWriteGuard<'a, HashMap<usize, Arc<AtomicBool>>>,
        probed_not_attached: usize,
    }
    unsafe extern "C" fn probe_handler(
        ctx: *mut c_void,
        trid: *const spdk_nvme_transport_id,
        opts: *mut spdk_nvme_ctrlr_opts,
    ) -> bool {
        // Safety: The memory layout of `*mut c_void` and `*mut ProbeCtx` are
        // the same, and is guaranteed to be the context which was sent to
        // `spdk_nvme_probe`.
        let ctx: *mut ProbeCtx = ctx.cast();
        let on_probe = &mut (*ctx).on_probe;
        let probed_not_attached = &mut (*ctx).probed_not_attached;
        let res = (*on_probe)(
            trid.as_ref().expect("trid is not null"),
            opts.as_mut().expect("opts is not null"),
        );
        if res {
            *probed_not_attached += 1;
        }
        res
    }
    unsafe extern "C" fn attach_handler(
        ctx: *mut c_void,
        trid: *const spdk_nvme_transport_id,
        handle: *mut spdk_nvme_ctrlr,
        opts: *const spdk_nvme_ctrlr_opts,
    ) {
        // Safety: The memory layout of `*mut c_void` and `*mut ProbeCtx` are
        // the same, and is guaranteed to be the context which was sent to
        // `spdk_nvme_probe`.
        let ctx: *mut ProbeCtx = ctx.cast();
        let on_attach = &mut (*ctx).on_attach;
        let devs = &mut (*ctx).devs;
        let probed_not_attached = &mut (*ctx).probed_not_attached;
        let ctrlr = Controller::steal_ptr(handle);

        // Insert into the global devices dictionary, incrementing the atomic
        // counter.
        devs.insert(handle as usize, ctrlr.removed.clone());

        *probed_not_attached -= 1;

        (*on_attach)(
            trid.as_ref().expect("trid is not null"),
            ctrlr,
            opts.as_ref().expect("opts is not null"),
        );
    }
    unsafe extern "C" fn remove_handler(ctx: *mut c_void, handle: *mut spdk_nvme_ctrlr) {
        let closures: *mut ProbeCtx = ctx.cast();
        let devs = &mut (*closures).devs;
        if let Some(removed) = devs.remove(&(handle as usize)) {
            // It is only ever changed from false to true, and it is only ever
            // initialized to false from a single thread. Relaxed is sufficient.
            removed.store(true, Ordering::Relaxed);
        }
    }

    let removed_refs = REMOVED_REFS.write().await;
    let mut ctx: ProbeCtx = ProbeCtx {
        on_probe: &mut on_probe,
        on_attach: &mut on_attach,
        devs: removed_refs,
        probed_not_attached: 0,
    };

    // Safety: `spdk_nvme_probe_async` per the documentation will call the
    // callback functions only when `spdk_nvme_probe_poll_async` is called, and we
    // drive the polling function to completion. So, it is safe to give it
    // references to the closures which are owned by this function.
    unsafe {
        // TODO: Implement proper trid builder.
        let mut trid = MaybeUninit::zeroed();
        spdk_nvme_trid_populate_transport(
            trid.as_mut_ptr(),
            spdk_nvme_transport_type::SPDK_NVME_TRANSPORT_PCIE,
        );
        trid.assume_init();
        let probe_ctx = spdk_nvme_probe_async(
            trid.as_ptr(),
            ptr::addr_of_mut!(ctx) as *mut c_void,
            Some(probe_handler),
            Some(attach_handler),
            Some(remove_handler),
        );
        while spdk_nvme_probe_poll_async(probe_ctx) != 0 {
            yield_now().await;
        }
    }
    if ctx.probed_not_attached > 0 {
        Err(Error::Probe)
    } else {
        Ok(())
    }
}

/// Probe the system for viable devices to which the SPDK driver should attach,
/// returning all of the attached controllers.
///
/// * `on_probe` - Return `true` if the given device should be attached.
pub async fn probe_collect<P>(on_probe: P) -> Result<Vec<Controller>>
where
    P: FnMut(&spdk_nvme_transport_id, &mut spdk_nvme_ctrlr_opts) -> bool,
{
    let mut res = vec![];
    probe(on_probe, |_trid, ctrlr, _opts| {
        res.push(ctrlr);
    })
    .await
    .map(|_| res)
}

/// Probe the system for viable devices to which the SPDK driver should attach.
pub async fn probe_all() -> Result<Vec<Controller>> {
    probe_collect(|_, _| true).await
}
