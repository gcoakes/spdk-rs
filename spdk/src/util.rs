use std::{
    cell::RefCell,
    ffi::c_void,
    future::Future,
    rc::{Rc, Weak},
    task::{Poll, Waker},
};

use spdk_sys::spdk_nvme_cpl;

pub struct DropBox<T> {
    waker: Rc<RefCell<Option<Waker>>>,
    pub dest: Rc<RefCell<Option<T>>>,
}

impl<T> DropBox<T> {
    pub fn new() -> Self {
        Self {
            waker: Rc::new(RefCell::new(None)),
            dest: Rc::new(RefCell::new(None)),
        }
    }

    pub fn waker_pair(&self) -> (Weak<RefCell<Option<Waker>>>, Weak<RefCell<Option<T>>>) {
        (Rc::downgrade(&self.waker), Rc::downgrade(&self.dest))
    }
}

impl<T> Future for DropBox<T> {
    type Output = T;

    fn poll(
        self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Self::Output> {
        if self.dest.borrow().is_some() {
            return Poll::Ready(self.dest.replace(None).unwrap());
        }
        self.waker.replace(Some(cx.waker().clone()));
        Poll::Pending
    }
}

pub unsafe extern "C" fn cmd_cb<F>(ctx: *mut c_void, cpl: *const spdk_nvme_cpl)
where
    F: FnOnce(&nvme::CompletionQueueEntry) + 'static,
{
    let ctx: Box<F> = Box::from_raw(ctx as *mut _);
    ctx(&*cpl.cast());
}
