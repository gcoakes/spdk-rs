use std::fmt::Debug;

pub type Result<T> = std::result::Result<T, Error>;

#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("failed to initialize SPDK env: {0}")]
    EnvInit(i32),
    #[error("failed to attach one or more probed devices")]
    Probe,
    #[error("failed to detach SPDK ctrlr: {0}")]
    Detach(i32),
    #[error("transport layer error")]
    Transport,
    #[error("memory allocation error")]
    MemoryAllocation,
    #[error("NVMe command is malformated")]
    CmdMalformed,
}
