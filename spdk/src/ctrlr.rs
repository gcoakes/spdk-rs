use std::{
    convert::Infallible,
    future::Future,
    marker::PhantomData,
    ptr,
    sync::{atomic::AtomicBool, Arc},
};

use futures_lite::future::{yield_now, FutureExt};
use nvme::{Cmd, CompletionQueueEntry};
use spdk_sys::*;

use crate::{
    util::{cmd_cb, DropBox},
    Buffer, Error, Namespace, Qpair, Result,
};

#[derive(Debug)]
pub struct Controller {
    handle: Option<*mut spdk_nvme_ctrlr>,
    pub removed: Arc<AtomicBool>,
    phantom: PhantomData<spdk_nvme_ctrlr>,
}

// Safety: Special care has been taken to ensure that all functions which take a
// `Controller` follow the threading guidelines from the upstream documentation.
//
// Note: Some functions (such as `spdk_nvme_alloc_io_qpair`) utilize an internal
// lock which is a part of the opaque handle within `Controller`.
unsafe impl Send for Controller {}
unsafe impl Sync for Controller {}

impl Drop for Controller {
    fn drop(&mut self) {
        // Since we have no viable recovery method from with the `Drop` trait,
        // we simply panic if unable to detach. To account for this, the pointer
        // may be nulled by an explicit call to `SpdkCtrl::detach` which is
        // fallible.
        self.detach().expect("detach SPDK ctrlr");
    }
}

impl Controller {
    pub unsafe fn steal_ptr(handle: *mut spdk_nvme_ctrlr) -> Self {
        Self {
            handle: Some(handle),
            removed: Arc::new(AtomicBool::new(false)),
            phantom: PhantomData,
        }
    }

    #[inline]
    pub fn attached_ptr(&self) -> *mut spdk_nvme_ctrlr {
        self.handle.expect("attached ctrlr")
    }

    /// Detach the controller from the driver. This can safely be called
    /// multiple times.
    pub fn detach(&mut self) -> Result<()> {
        if let Some(ctrlr) = self.handle {
            // Safety: `SpdkCtrlr` takes ownership of the allocated resources for
            // spdk_nvme_ctrlr; however, actually deallocation should be performed
            // by `spdk_nvme_detach`.
            let res = unsafe { spdk_nvme_detach(ctrlr) };
            if res != 0 {
                return Err(Error::Detach(res));
            }
        }
        self.handle = None;
        Ok(())
    }

    pub fn ctrlr_data(&self) -> &nvme::IdCtrl {
        // Safety: The documentation indicates this function can safely be
        // called at any point from any thread while the controller is attached
        // to the driver.
        unsafe { &*spdk_nvme_ctrlr_get_data(self.attached_ptr()).cast() }
    }

    pub fn iter_active_ns(&self) -> SpdkCtrlrIterActiveNs {
        SpdkCtrlrIterActiveNs {
            current_nsid: 0,
            ctrlr: self,
        }
    }

    pub fn new_qpair(&self) -> Qpair {
        // Safety: Upstream documentation indicates this does not have a failure
        // condition.
        unsafe {
            Qpair::steal_ptr(spdk_nvme_ctrlr_alloc_io_qpair(
                self.attached_ptr(),
                ptr::null(),
                0,
            ))
        }
    }

    pub fn new_buffer(&self, size: size_t, alignment: size_t) -> Buffer {
        Buffer::on_ctrlr(self, size, alignment)
    }

    fn io_cmd<F>(
        &self,
        qpair: &Qpair,
        cmd: &mut Cmd,
        buffer: &mut Buffer,
        callback: F,
    ) -> Result<()>
    where
        F: FnOnce(&nvme::CompletionQueueEntry) + 'static,
    {
        // This function is generic over F and doesn't use dyn F, so no need for
        // double indirection à la
        // https://stackoverflow.com/questions/32270030/how-do-i-convert-a-rust-closure-to-a-c-style-callback.
        let ctx = Box::new(callback);
        let res = unsafe {
            spdk_nvme_ctrlr_cmd_io_raw(
                self.attached_ptr(),
                qpair.ptr,
                cmd as *mut _ as *mut spdk_nvme_cmd,
                buffer.ptr,
                buffer.size as u32,
                Some(cmd_cb::<F>),
                Box::into_raw(ctx).cast(),
            )
        };
        match -res {
            errno::ENOMEM => Err(Error::MemoryAllocation),
            errno::ENXIO => Err(Error::Transport),
            // This handles both 0 and -EFAULT. -EFAULT must be handled on
            // the completion entry side because the documentation indicates
            // it will still call the callback function.
            _ => Ok(()),
        }
    }

    pub fn aio_cmd(
        &self,
        qpair: &Qpair,
        cmd: &mut Cmd,
        buffer: &mut Buffer,
    ) -> DropBox<Result<CompletionQueueEntry>> {
        let dropbox = DropBox::new();
        let (waker, dest) = dropbox.waker_pair();
        let res = self.io_cmd(qpair, cmd, buffer, move |cpl| {
            if let Some(dest) = dest.upgrade() {
                dest.replace(Some(Ok(*cpl)));
            }
            if let Some(w) = waker.upgrade().and_then(|w| w.replace(None)) {
                w.wake()
            }
        });
        if let Err(err) = res {
            dropbox.dest.replace(Some(Err(err)));
        }
        dropbox
    }

    fn admin_cmd<F>(&self, cmd: &mut Cmd, buffer: Option<&mut Buffer>, callback: F) -> Result<()>
    where
        F: FnOnce(&nvme::CompletionQueueEntry) + 'static,
    {
        // This function is generic over F and doesn't use dyn F, so no need for
        // double indirection à la
        // https://stackoverflow.com/questions/32270030/how-do-i-convert-a-rust-closure-to-a-c-style-callback.
        let ctx = Box::new(callback);
        let res = unsafe {
            spdk_nvme_ctrlr_cmd_admin_raw(
                self.attached_ptr(),
                cmd as *mut _ as *mut spdk_nvme_cmd,
                buffer.as_ref().map(|b| b.ptr).unwrap_or(ptr::null_mut()),
                buffer.as_ref().map(|b| b.size).unwrap_or(0) as u32,
                Some(cmd_cb::<F>),
                Box::into_raw(ctx).cast(),
            )
        };
        match -res {
            errno::ENXIO => Err(Error::Transport),
            _ => Ok(()),
        }
    }

    pub fn aadmin_cmd(
        &self,
        cmd: &mut Cmd,
        buffer: Option<&mut Buffer>,
    ) -> DropBox<Result<CompletionQueueEntry>> {
        let dropbox = DropBox::new();
        let (waker, dest) = dropbox.waker_pair();
        let res = self.admin_cmd(cmd, buffer, move |cpl| {
            if let Some(dest) = dest.upgrade() {
                dest.replace(Some(Ok(*cpl)));
            }
            if let Some(w) = waker.upgrade().and_then(|w| w.replace(None)) {
                w.wake()
            }
        });
        if let Err(err) = res {
            dropbox.dest.replace(Some(Err(err)));
        }
        dropbox
    }

    pub fn process_completions(&self) -> i32 {
        // Safety: `self` will outlive the FFI call.
        unsafe { spdk_nvme_ctrlr_process_admin_completions(self.attached_ptr()) }
    }

    /// Indefinitely process completions, yielding to the async executor
    /// periodically.
    pub async fn process_completions_loop(&self) -> Result<Infallible> {
        loop {
            let res = self.process_completions();
            if res == -errno::ENXIO {
                return Err(Error::Transport);
            }
            yield_now().await;
        }
    }

    pub async fn process_admin<'a, T, F, R>(&'a self, admin: F) -> Result<T>
    where
        F: FnOnce(&'a Self) -> R,
        R: Future<Output = Result<T>>,
    {
        let proc_loop = self.process_completions_loop();
        let admin_fut = admin(self);
        admin_fut
            .race(async {
                // unimplemented because the process loop will never return unless
                // there is an error. So, the Ok type of the Result is irrelevant,
                // but would otherwise break the type checker.
                proc_loop.await.map(|_| unimplemented!())
            })
            .await
    }

    pub async fn process<'a, T, F, R>(&'a self, qpair: &'a Qpair, io: F) -> Result<T>
    where
        F: FnOnce(&'a Self, &'a Qpair) -> R,
        R: Future<Output = Result<T>>,
    {
        let admin_loop = self.process_completions_loop();
        let proc_loop = qpair.process_completions_loop();
        io(self, qpair)
            .race(async { admin_loop.await.map(|_| unimplemented!()) })
            .race(async { proc_loop.await.map(|_| unimplemented!()) })
            .await
    }
}

pub struct SpdkCtrlrIterActiveNs<'a> {
    current_nsid: u32,
    ctrlr: &'a Controller,
}

impl<'a> Iterator for SpdkCtrlrIterActiveNs<'a> {
    type Item = Namespace;

    fn next(&mut self) -> Option<Self::Item> {
        self.current_nsid = if self.current_nsid == 0 {
            // Safety: `ctrlr.attached_ptr()` guarantees to produce a valid
            // controller pointer or panic.
            unsafe { spdk_nvme_ctrlr_get_first_active_ns(self.ctrlr.attached_ptr()) }
        } else {
            // Safety: `ctrlr.attached_ptr()` guarantees to produce a valid
            // controller pointer or panic.
            unsafe {
                spdk_nvme_ctrlr_get_next_active_ns(self.ctrlr.attached_ptr(), self.current_nsid)
            }
        };
        if self.current_nsid > 0 {
            // Safety: `ctrlr.attached_ptr()` guarantees to produce a valid
            // controller pointer or panic. Also, upstream documentation
            // indicates there is no failure case for this function.
            Some(unsafe {
                Namespace::steal_ptr(spdk_nvme_ctrlr_get_ns(
                    self.ctrlr.attached_ptr(),
                    self.current_nsid,
                ))
            })
        } else {
            None
        }
    }
}
