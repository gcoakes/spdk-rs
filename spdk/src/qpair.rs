use std::{convert::Infallible, future::Future, marker::PhantomData};

use futures_lite::future::{yield_now, FutureExt};
use spdk_sys::*;

use crate::{Error, Result};

pub struct Qpair {
    pub ptr: *mut spdk_nvme_qpair,
    phantom: PhantomData<spdk_nvme_qpair>,
}

impl Drop for Qpair {
    fn drop(&mut self) {
        // Safety: `self.ptr` is exclusively owned by `self` and was allocated
        // by SPDK.
        unsafe {
            spdk_nvme_ctrlr_free_io_qpair(self.ptr);
        }
    }
}

impl Qpair {
    pub unsafe fn steal_ptr(qpair: *mut spdk_nvme_qpair) -> Self {
        Self {
            ptr: qpair,
            phantom: PhantomData,
        }
    }

    pub fn process_completions(&self, max_comp: u32) -> i32 {
        // Safety: `self` will outlive the FFI call.
        unsafe { spdk_nvme_qpair_process_completions(self.ptr, max_comp) }
    }

    /// Indefinitely process completions, yielding to the async executor
    /// periodically.
    pub async fn process_completions_loop(&self) -> Result<Infallible> {
        loop {
            let res = self.process_completions(0);
            if res == -errno::ENXIO {
                return Err(Error::Transport);
            }
            yield_now().await;
        }
    }

    pub async fn process<'a, T, F, R>(&'a self, io: F) -> Result<T>
    where
        F: FnOnce(&'a Qpair) -> R,
        R: Future<Output = Result<T>>,
    {
        let proc_loop = self.process_completions_loop();
        let io_fut = io(self);
        io_fut
            .race(async {
                // unimplemented because the process loop will never return unless
                // there is an error. So, the Ok type of the Result is irrelevant,
                // but would otherwise break the type checker.
                proc_loop.await.map(|_| unimplemented!())
            })
            .await
    }
}
