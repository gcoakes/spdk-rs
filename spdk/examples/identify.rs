/*! Cooperatively send the identify controller command to all controllers. */

// Workaround for https://github.com/rust-lang/rust/issues/88085
// This ensures the constructor function for registering nvme transport methods
// are included in the final binary.
#![feature(native_link_modifiers)]
#![feature(native_link_modifiers_whole_archive)]
#[link(name = "spdk_nvme", kind = "static", modifiers = "+whole-archive")]
extern "C" {}

use nvme::{Cmd, FromBytes, IdCtrl};
use smol::LocalExecutor;

#[smol_potat::main]
async fn main() -> spdk::Result<()> {
    spdk::Env::new().name(b"identify\0").shm_id(1).init()?;

    let ex = LocalExecutor::new();

    let ctrlrs = spdk::probe_all().await?;

    for ctrlr in ctrlrs {
        let admin_task = ex.spawn(async move {
            ctrlr
                .process_admin(|ctrlr| async move {
                    let mut cmd = Cmd::builder().cdw0(6).cdw10(1).build();
                    let mut buffer = spdk::Buffer::new(0x1000, 0x1000);
                    let res = ctrlr.aadmin_cmd(&mut cmd, Some(&mut buffer)).await?;
                    println!("{:?}", res.status_field().status_code());
                    let id_ctrl = IdCtrl::from_bytes(buffer.as_ref()).unwrap();
                    println!("SN: {}", id_ctrl.sn);
                    println!("MN: {}", id_ctrl.mn);
                    Ok(())
                })
                .await
        });
        ex.spawn(admin_task).detach();
    }

    while ex.try_tick() {}

    Ok(())
}
