/*! Cooperatively write "Hello, world!" to LBA 0 of every namespace on every controller found by SPDK. */

// Workaround for https://github.com/rust-lang/rust/issues/88085
// This ensures the constructor function for registering nvme transport methods
// are included in the final binary.
#![feature(native_link_modifiers)]
#![feature(native_link_modifiers_whole_archive)]
#[link(name = "spdk_nvme", kind = "static", modifiers = "+whole-archive")]
extern "C" {}

use std::io::{Cursor, Write};

use smol::LocalExecutor;

#[smol_potat::main]
async fn main() -> spdk::Result<()> {
    spdk::Env::new().name(b"hello_world\0").shm_id(1).init()?;

    let ctrlrs = spdk::probe_all().await?;

    let ex = LocalExecutor::new();

    for ctrlr in ctrlrs.iter() {
        // Print some info identifying the controller.
        let cdata: &nvme::IdCtrl = ctrlr.ctrlr_data();
        println!("Device: {} ({})", cdata.mn, cdata.sn);

        for ns in ctrlr.iter_active_ns() {
            // Allocate and new Qpair for this controller.
            // This happens synchronously.
            let qpair = ctrlr.new_qpair();

            let io_fut = async move {
                // Run some I/O against it in a cooperative manner.
                // `Qpair::process` will race the process completions queue loop
                // against the provided closure.
                qpair
                    .process(|qpair| async move {
                        println!("  Namespace ID: {} size: {}GB", ns.nsid(), ns.size());

                        // Create a SPDK buffer. This is a special type of memory that
                        // will be accessible by both the NVMe device and the process.
                        let mut buf = spdk::Buffer::new(0x1000, 0x1000);

                        // Wrap it in a cursor, so we can treat it like a file and just
                        // write bytes to it.
                        let mut cursor = Cursor::new(buf.as_mut());
                        cursor.write(b"Hello, world!").expect("write to buffer");

                        // Send the write I/O command and await the completion queue
                        // response. This is the magic. It will cooperatively wait for
                        // the response from the NVMe device, allowing other I/O
                        // pathways to progress on this same thread.
                        let cpl = ns.awrite(qpair, &mut buf, 0, 1, 0).await?;
                        println!("  Write completed: {:?}", cpl.status_field().status_code());

                        // Create another buffer to read the same LBA back and verify it
                        // has the correct data. We could also just zero out the old
                        // buffer and re-use, but for demonstration purposes, this'll
                        // work.
                        let mut buf = spdk::Buffer::new(0x1000, 0x1000);

                        // Same thing as before. This will cooperatively wait for the
                        // response.
                        let cpl = ns.aread(qpair, &mut buf, 0, 1, 0).await?;
                        println!(
                            "  Read completed: {:?}, {}",
                            cpl.status_field().status_code(),
                            String::from_utf8_lossy(&buf[..13])
                        );
                        Ok(())
                    })
                    .await
            };
            ex.spawn(io_fut).detach();
        }
    }

    while ex.try_tick() {}

    Ok(())
}
