/*! Run a random read/write workload against all namespaces.

Architecture:

Each controller is allocated with N qpairs and threads.

Each thread has ownership of the qpair.

Each thread has ownership of a rng which is seeded from the command line.

All threads pass an I/O unit completion message to the main thread for every lba
involved with the I/O unit.

The main thread tracks the possible states of each lba's hash based upon write
completion messages that have arrived since the last read or the last read hash.

When a read completion message arrives, if the read hash matches any of the
possible states, then the lba is considered verified.

All hashes are xxHash3 64-bit.

After all I/O paths have completed, the remaining journal entries are verified.

TODO: Will writes/reads be sequentially consistent with the journal?
TODO: Will the memory usage of the journal be too much?
*/

// Workaround for https://github.com/rust-lang/rust/issues/88085
// This ensures the constructor function for registering nvme transport methods
// are included in the final binary.
#![feature(native_link_modifiers)]
#![feature(native_link_modifiers_whole_archive)]
#[link(name = "spdk_nvme", kind = "static", modifiers = "+whole-archive")]
extern "C" {}

use std::{
    cell::{RefCell, RefMut},
    cmp::min,
    collections::{HashMap, HashSet},
    str::FromStr,
    sync::Arc,
    thread,
};

use futures_util::stream::FuturesUnordered;
use nvme::StatusField;
use rand::{distributions::Uniform, prelude::*};
use rand_chacha::{
    rand_core::{RngCore, SeedableRng},
    ChaCha8Rng,
};
use smol::{
    channel::{self, Sender},
    future,
    prelude::*,
    LocalExecutor,
};

use spdk::{Buffer, Namespace, Qpair, Result};
use structopt::StructOpt;
use xxhash_rust::xxh3::xxh3_64;

#[derive(Debug, StructOpt, Clone, Copy)]
struct Opts {
    #[structopt(default_value = "1", short = "t", long = "threads")]
    threads: usize,
    #[structopt(default_value = "32", short = "q", long = "qdepth")]
    qdepth: usize,
    #[structopt(short = "s", long = "seed")]
    seed: Option<u64>,
    #[structopt(
        default_value = "1:1",
        long = "rwratio",
        parse(try_from_str = parse_ratio)
    )]
    rwratio: (f64, f64),
    // TODO: Configurable distributions based upon `rand::distributions`.
    #[structopt(default_value = "1", long = "nlb-min")]
    nlb_min: u16,
    #[structopt(default_value = "1", long = "nlb-max")]
    nlb_max: u16,
}

#[derive(Debug)]
enum Msg {
    Read {
        status: StatusField,
        lba: u64,
        hash: u64,
    },
    Write {
        status: StatusField,
        lba: u64,
        hash: u64,
    },
}

#[derive(Debug)]
struct Miscompare {
    pub lba: u64,
    pub prev: HashSet<u64>,
    pub new: u64,
}

#[smol_potat::main]
async fn main() -> Result<()> {
    let opts = Opts::from_args();

    let mut rng = match opts.seed {
        Some(seed) => ChaCha8Rng::seed_from_u64(seed),
        None => ChaCha8Rng::from_entropy(),
    };

    spdk::Env::new().name(b"randrw\0").shm_id(1).init()?;

    let ctrlrs: Vec<_> = spdk::probe_all().await?.drain(..).map(Arc::new).collect();

    let mut threads = vec![];

    let (tx, rx) = channel::unbounded::<Msg>();

    for ctrlr in ctrlrs.iter() {
        // Print some info identifying the controller.
        let cdata: &nvme::IdCtrl = ctrlr.ctrlr_data();
        println!("Device: {} ({})", cdata.mn, cdata.sn);

        for ns in ctrlr.iter_active_ns().map(Arc::new) {
            for _ in 0..opts.threads {
                let seed = rng.next_u64();
                let ctrlr_ref = ctrlr.clone();
                let ns_ref = ns.clone();
                let thread_tx = tx.clone();
                let thread = thread::spawn(move || {
                    io_thread(&opts, thread_tx, ns_ref, ctrlr_ref.new_qpair(), seed)
                });
                threads.push(thread);
            }
        }
    }

    let mut journal: HashMap<u64, HashSet<_>> = HashMap::new();
    let mut miscompares = vec![];

    while let Ok(msg) = rx.recv().await {
        match msg {
            Msg::Read { lba, hash, .. } => match journal.get_mut(&lba) {
                Some(old_hashes) => {
                    if old_hashes.contains(&hash) {
                        old_hashes.clear();
                    } else {
                        let miscompare = Miscompare {
                            lba,
                            prev: old_hashes.clone(),
                            new: hash,
                        };
                        println!("{:?}", miscompare);
                        miscompares.push(miscompare);
                    }
                    old_hashes.insert(hash);
                }
                None => {
                    journal.insert(lba, HashSet::from([hash]));
                }
            },
            Msg::Write { lba, hash, .. } => match journal.get_mut(&lba) {
                Some(old_hashes) => {
                    old_hashes.insert(hash);
                }
                None => {
                    journal.insert(lba, HashSet::from([hash]));
                }
            },
        }
    }

    for thread in threads {
        thread.join().unwrap()?;
    }

    Ok(())
}

fn io_thread(
    opts: &Opts,
    tx: Sender<Msg>,
    ns: Arc<Namespace>,
    qpair: Qpair,
    seed: u64,
) -> Result<()> {
    let ns_data = ns.ns_data();
    let lbads = 2u64.pow(ns_data.lbafs[ns_data.flbas() as usize].lbads() as u32);

    let max_buf_size = lbads * opts.nlb_max as u64;
    let buffers: Vec<_> = (0..opts.qdepth)
        .map(|_| Buffer::new(max_buf_size, lbads))
        .map(RefCell::new)
        .collect();

    let mut rng = ChaCha8Rng::seed_from_u64(seed);

    // Produces true if write, false if read.
    let choose_rw = Uniform::from(-opts.rwratio.0..=opts.rwratio.1).map(|x| x > 0.0);

    // Produces the lba to or from which data should be written or read.
    let choose_lba = Uniform::from(0..ns_data.nsze());

    // Produces the number of lbas which should be written or read.
    let choose_nlb = Uniform::from(opts.nlb_min..=opts.nlb_max);

    let ex = LocalExecutor::new();
    ex.spawn(qpair.process_completions_loop()).detach();

    future::block_on(ex.run(async {
        let mut io_tasks = FuturesUnordered::new();
        let mut next_buf = 0;
        loop {
            while io_tasks.len() < opts.qdepth {
                let lba = rng.sample(&choose_lba);
                let nlb = min(
                    rng.sample(&choose_nlb),
                    min(ns_data.nsze() - lba, u16::MAX as u64) as u16,
                );
                let mut buf = buffers[next_buf].borrow_mut();
                let read = rng.sample(&choose_rw);
                if !read {
                    rng.fill_bytes(&mut buf[..(nlb as u64 * lbads) as usize]);
                }
                io_tasks.push(
                    io_unit(
                        &tx, read, &ns, &qpair, next_buf, buf, lba, nlb as u32, lbads,
                    )
                    .boxed_local(),
                );
                // Only relevant when initially adding tasks.
                next_buf += 1;
            }
            match io_tasks.next().await {
                Some(Ok(buf_id)) => next_buf = buf_id,
                Some(Err(err)) => return Err(err),
                None => break,
            }
        }
        Ok(())
    }))
}

async fn io_unit<'a>(
    tx: &'a Sender<Msg>,
    read: bool,
    ns: &'a Namespace,
    qpair: &'a Qpair,
    buf_id: usize,
    mut buf: RefMut<'a, Buffer>,
    lba: u64,
    nlb: u32,
    lbads: u64,
) -> Result<usize> {
    if nlb == 0 {
        println!("nlb: {}", nlb);
    }
    let cpl = if read {
        ns.aread(qpair, &mut *buf, lba, nlb, 0).await?
    } else {
        ns.awrite(qpair, &mut *buf, lba, nlb, 0).await?
    };
    for (idx, lba) in (lba..lba + nlb as u64).enumerate() {
        let start = idx * lbads as usize;
        let end = start + lbads as usize;
        let hash = xxh3_64(&mut buf[start..end]);
        let msg = if read {
            Msg::Read {
                status: cpl.status_field(),
                lba,
                hash,
            }
        } else {
            Msg::Write {
                status: cpl.status_field(),
                lba,
                hash,
            }
        };
        tx.send(msg).await.expect("send msg");
    }
    Ok(buf_id)
}

fn parse_ratio<T: FromStr>(src: &str) -> std::result::Result<(T, T), <T as FromStr>::Err> {
    let (left, right) = src.split_once(":").unwrap();
    Ok((left.parse()?, right.parse()?))
}
