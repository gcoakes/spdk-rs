#![allow(unused_braces)]

#[macro_use]
extern crate transmute_tools;

mod ident;
#[doc(inline)]
pub use ident::*;
mod logpage;
#[doc(inline)]
pub use logpage::*;
mod completion;
#[doc(inline)]
pub use completion::CompletionQueueEntry;
mod status;
#[doc(inline)]
pub use status::{
    CmdSpecificStatus, GenericStatus, MadIntegrityStatus, PathRelatedStatus, StatusCode,
    StatusCodeType, StatusField,
};
mod cmd;
#[doc(inline)]
pub use cmd::*;
mod util;
#[doc(inline)]
pub use util::{FixedStr, FromBytes, Reserved, TransmuteSafe};
