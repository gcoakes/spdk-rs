use crate::{StatusField, TransmuteSafe};

#[repr(C)]
#[endianness(le)]
#[test_structure(size = 16)]
#[derive(Clone, Copy, Debug)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct CompletionQueueEntry {
    #[loc(0:3)]
    pub dw0: u32,
    #[loc(4:7)]
    pub dw1: u32,
    #[loc(8:9)]
    pub sqhd: u16,
    #[loc(10:11)]
    pub sqid: u16,
    #[loc(12:13)]
    pub cid: u16,
    #[loc(14:15)]
    pub sf: u16,
}

// Safety: `CompletionQueueEntry` contains valid fields for all possible
// similarly sized byte slices. Special care has been taken to ensure all used
// enums fill their repr space entirely.
unsafe impl TransmuteSafe for CompletionQueueEntry {}

impl CompletionQueueEntry {
    pub fn status_field(&self) -> StatusField {
        StatusField::from_bytes(self.sf.to_le_bytes())
    }
}
