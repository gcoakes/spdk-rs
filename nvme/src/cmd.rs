use crate::TransmuteSafe;

#[endianness(le)]
#[test_structure(size = 64)]
#[repr(C)]
#[derive(Default, Debug, Clone, Copy)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct Cmd {
    #[loc(0:3)]
    pub cdw0: u32,
    #[log(4:7)]
    pub nsid: u32,
    #[loc(8:15)]
    #[cfg_attr(feature = "serde", serde(skip))]
    __rsvd8: [u8; 8],
    #[loc(16:23)]
    pub mptr: u64,
    #[loc(24:39)]
    pub dptr: u128,
    #[loc(40:43)]
    pub cdw10: u32,
    #[loc(44:47)]
    pub cdw11: u32,
    #[loc(48:51)]
    pub cdw12: u32,
    #[loc(52:55)]
    pub cdw13: u32,
    #[loc(56:59)]
    pub cdw14: u32,
    #[loc(60:63)]
    pub cdw15: u32,
}

// Safety: `Cmd` contains valid fields for all possible similarly sized byte
// slices. Special care has been taken to ensure all used enums fill their repr
// space entirely.
unsafe impl TransmuteSafe for Cmd {}

impl Cmd {
    pub fn builder() -> CmdBuilder {
        CmdBuilder(Cmd {
            ..Default::default()
        })
    }
}

#[repr(transparent)]
#[derive(Clone, Copy)]
pub struct CmdBuilder(Cmd);

impl CmdBuilder {
    pub fn cdw0(&mut self, val: u32) -> &mut Self {
        self.0.set_cdw0(val);
        self
    }
    pub fn nsid(&mut self, val: u32) -> &mut Self {
        self.0.set_nsid(val);
        self
    }
    pub fn mptr(&mut self, val: u64) -> &mut Self {
        self.0.set_mptr(val);
        self
    }
    pub fn dptr(&mut self, val: u128) -> &mut Self {
        self.0.set_dptr(val);
        self
    }
    pub fn cdw10(&mut self, val: u32) -> &mut Self {
        self.0.set_cdw10(val);
        self
    }
    pub fn cdw11(&mut self, val: u32) -> &mut Self {
        self.0.set_cdw11(val);
        self
    }
    pub fn cdw12(&mut self, val: u32) -> &mut Self {
        self.0.set_cdw12(val);
        self
    }
    pub fn cdw13(&mut self, val: u32) -> &mut Self {
        self.0.set_cdw13(val);
        self
    }
    pub fn cdw14(&mut self, val: u32) -> &mut Self {
        self.0.set_cdw14(val);
        self
    }
    pub fn cdw15(&mut self, val: u32) -> &mut Self {
        self.0.set_cdw15(val);
        self
    }
    pub fn build(self) -> Cmd {
        self.0
    }
}
