use std::{
    borrow::Cow,
    convert::{TryFrom, TryInto},
    fmt::Display,
    mem::MaybeUninit,
};

pub trait FromBytes {
    fn from_bytes<'a>(bytes: &'a [u8]) -> Result<&'a Self, usize>;
    /// Implementors of this function must ensure the following invariants are respected:
    ///
    /// 1. `Self` must have an alignment of 1.
    /// 2. Where Self: Sized, `mem::size_of::<Self>()` must be equal to `bytes.len()`.
    /// 3. Where Self is a slice of some type T, this must be true `bytes.len() % mem::size_of::<T>() == 0`.
    unsafe fn from_bytes_unchecked<'a>(bytes: &'a [u8]) -> &'a Self;
}

/// Marker trait to indicate that struct can safely be transmuted or cast from
/// bytes without any undefined behavior. This means any contained enums fill
/// their entire repr space. Also, this most likely means the struct is packed.
pub unsafe trait TransmuteSafe {}

impl<T> FromBytes for T
where
    T: TransmuteSafe + Sized,
{
    fn from_bytes<'a>(bytes: &'a [u8]) -> Result<&'a Self, usize> {
        if bytes.len() == std::mem::size_of::<Self>() {
            // Safety: Implementing this on a type is effectively a contract
            // between the struct writer and this trait indicating it satifies
            // the invariants listed above.
            Ok(unsafe { Self::from_bytes_unchecked(bytes) })
        } else {
            Err(bytes.len())
        }
    }

    unsafe fn from_bytes_unchecked<'a>(bytes: &'a [u8]) -> &'a Self {
        &*bytes.as_ptr().cast()
    }
}

impl<T> FromBytes for [T]
where
    T: TransmuteSafe + Sized,
{
    fn from_bytes<'a>(bytes: &'a [u8]) -> Result<&'a Self, usize> {
        if bytes.len() % std::mem::size_of::<T>() == 0 {
            // Safety: Implementing this on a type is effectively a contract
            // between the struct writer and this trait indicating it satifies
            // the invariants listed above.
            Ok(unsafe { Self::from_bytes_unchecked(bytes) })
        } else {
            Err(bytes.len())
        }
    }

    unsafe fn from_bytes_unchecked<'a>(bytes: &'a [u8]) -> &'a Self {
        std::slice::from_raw_parts(
            bytes.as_ptr() as *const T,
            bytes.len() / std::mem::size_of::<T>(),
        )
    }
}

#[repr(transparent)]
#[derive(Clone, Copy)]
pub struct Reserved<const SIZE: usize>([u8; SIZE]);

impl<const SIZE: usize> Default for Reserved<SIZE> {
    fn default() -> Self {
        Reserved([0; SIZE])
    }
}

#[repr(transparent)]
#[derive(Clone, Copy)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
// TODO: Implement serde without copying.
#[cfg_attr(feature = "serde", serde(try_from = "String", into = "String"))]
pub struct FixedStr<const SIZE: usize>([u8; SIZE]);

impl<const SIZE: usize> FixedStr<SIZE> {
    pub fn to_string_lossy(&self) -> Cow<str> {
        String::from_utf8_lossy(&self.0[..])
    }
}

impl<const SIZE: usize> Display for FixedStr<SIZE> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.to_string_lossy())
    }
}

impl<const SIZE: usize> From<FixedStr<SIZE>> for String {
    fn from(val: FixedStr<SIZE>) -> Self {
        val.to_string_lossy().to_string()
    }
}

impl<const SIZE: usize> TryFrom<&[u8]> for FixedStr<SIZE> {
    type Error = usize;

    fn try_from(bytes: &[u8]) -> Result<Self, Self::Error> {
        if bytes.len() != SIZE {
            Err(bytes.len())
        } else {
            // Safety: cpy is guaranteed to be initialized coming out of this
            // unsafe because it has previously been validated that bytes
            // contains exactly enough bytes to fill cpl.
            Ok(FixedStr(unsafe {
                let mut cpy: [u8; SIZE] = MaybeUninit::uninit().assume_init();
                cpy[..].clone_from_slice(bytes);
                cpy
            }))
        }
    }
}

impl<const SIZE: usize> TryFrom<String> for FixedStr<SIZE> {
    type Error = usize;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        value.as_bytes().try_into()
    }
}

#[repr(transparent)]
#[derive(Clone, Copy)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[cfg_attr(feature = "serde", serde(try_from = "Vec<bool>", into = "Vec<bool>"))]
pub struct BitArray<const BYTES: usize>([u8; BYTES]);

impl<const BYTES: usize> BitArray<BYTES> {
    const BITS: usize = BYTES * 8;
    pub fn get(&self, index: usize) -> Option<bool> {
        if index >= Self::BITS {
            None
        } else {
            Some(self.0[index / 8] & (1 << (index % 8)) != 0)
        }
    }

    pub fn set(&mut self, index: usize, val: bool) {
        let bit = 1 << (index % 8);
        if val {
            self.0[index / 8] |= bit;
        } else {
            self.0[index / 8] &= !bit;
        }
    }
}

impl<const BYTES: usize> TryFrom<Vec<bool>> for BitArray<BYTES> {
    type Error = usize;

    fn try_from(value: Vec<bool>) -> Result<Self, Self::Error> {
        if value.len() > Self::BITS {
            Err(value.len())
        } else {
            let mut res = Self([0; BYTES]);
            for (bit, val) in value.into_iter().enumerate() {
                res.set(bit, val);
            }
            Ok(res)
        }
    }
}

impl<const BYTES: usize> From<BitArray<BYTES>> for Vec<bool> {
    fn from(value: BitArray<BYTES>) -> Self {
        (0..BYTES * 8).filter_map(|idx| value.get(idx)).collect()
    }
}
