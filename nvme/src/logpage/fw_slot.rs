use std::borrow::Cow;

use crate::{FixedStr, Reserved, TransmuteSafe};

use modular_bitfield::prelude::*;

#[endianness(le)]
#[test_structure(size = 512)]
#[repr(C, align(1))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct FwSlotLog {
    #[loc(0:0)]
    #[cfg_attr(feature = "serde", serde(with = "ActiveFwInfoUnpacked"))]
    pub afi: ActiveFwInfo,
    #[loc(1:7)]
    #[cfg_attr(feature = "serde", serde(skip))]
    __rsvd1: Reserved<7>,
    #[loc(8:63)]
    pub frs: [FixedStr<8>; 7],
    #[loc(64:511)]
    #[cfg_attr(feature = "serde", serde(skip))]
    __rsvd64: Reserved<448>,
}

/// Safety: `FwSlotLog` is `repr(C, align(1))` and contains no references or
/// pointers.
unsafe impl TransmuteSafe for FwSlotLog {}

impl FwSlotLog {
    pub fn get_slot<'a>(&'a self, index: usize) -> Cow<'a, str> {
        self.frs[index].to_string_lossy()
    }
}

#[bitfield]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Clone, Copy)]
pub struct ActiveFwInfo {
    pub active_slot: B3,
    #[skip]
    __: B1,
    pub next_active: B3,
    #[skip]
    __: B1,
}

#[cfg(feature = "serde")]
#[derive(serde::Serialize, serde::Deserialize)]
#[serde(remote = "ActiveFwInfo")]
struct ActiveFwInfoUnpacked {
    #[serde(getter = "ActiveFwInfo::active_slot")]
    pub active_slot: u8,
    #[serde(getter = "ActiveFwInfo::next_active")]
    pub next_active: u8,
}

#[cfg(feature = "serde")]
impl From<ActiveFwInfoUnpacked> for ActiveFwInfo {
    fn from(unpacked: ActiveFwInfoUnpacked) -> Self {
        ActiveFwInfo::new()
            .with_active_slot(unpacked.active_slot)
            .with_next_active(unpacked.next_active)
    }
}
