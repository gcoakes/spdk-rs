use modular_bitfield::prelude::*;

use crate::{Reserved, TransmuteSafe};

#[endianness(le)]
#[test_structure(size = 4096)]
#[repr(C, align(1))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct IdNmsp {
    #[loc(0:7)]
    pub nsze: u64,
    #[loc(8:15)]
    pub ncap: u64,
    #[loc(16:23)]
    pub nuse: u64,
    #[loc(24:24)]
    pub nsfeat: u8,
    #[loc(25:25)]
    pub nlbaf: u8,
    #[loc(26:26)]
    pub flbas: u8,
    #[loc(27:27)]
    pub mc: u8,
    #[loc(28:28)]
    pub dpc: u8,
    #[loc(29:29)]
    pub dps: u8,
    #[loc(30:30)]
    pub nmic: u8,
    #[loc(31:31)]
    pub rescap: u8,
    #[loc(32:32)]
    pub fpi: u8,
    #[loc(33:33)]
    pub dlfeat: u8,
    #[loc(34:35)]
    pub nawun: u16,
    #[loc(36:37)]
    pub nawupf: u16,
    #[loc(38:39)]
    pub nacwu: u16,
    #[loc(40:41)]
    pub nabsn: u16,
    #[loc(42:43)]
    pub nabo: u16,
    #[loc(44:45)]
    pub nabspf: u16,
    #[loc(46:47)]
    pub noiob: u16,
    #[loc(48:63)]
    // TODO: Investigate the timeline for u128 ABI stabilization. Right now,
    // it's technically UB to use with FFI, though it does appear to work.
    pub nvmcap: u128,
    #[loc(64:65)]
    pub npwg: u16,
    #[loc(66:67)]
    pub npwa: u16,
    #[loc(68:69)]
    pub npdg: u16,
    #[loc(70:71)]
    pub npda: u16,
    #[loc(72:73)]
    pub nows: u16,
    #[serde(skip)]
    #[loc(74:91)]
    __rsvd74: Reserved<18>,
    #[loc(92:95)]
    pub anagrpid: u32,
    #[serde(skip)]
    #[loc(96:98)]
    __rsvd96: Reserved<3>,
    #[loc(99:99)]
    pub nsattr: u8,
    #[loc(100:101)]
    pub nvmsetid: u16,
    #[loc(102:103)]
    pub endgid: u16,
    #[loc(104:119)]
    pub nguid: u128,
    #[loc(120:127)]
    pub eui64: u64,
    #[loc(128:191)]
    pub lbafs: [LbaFormat; 16],
    #[serde(skip)]
    #[loc(192:4095)]
    __unimplemented128: Reserved<3904>,
}

/// Safety: `IdNmsp` is `repr(C, align(1))` and contains no references or
/// pointers.
unsafe impl TransmuteSafe for IdNmsp {}

#[bitfield]
#[repr(u32)]
#[derive(BitfieldSpecifier, Clone, Copy, Debug)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct LbaFormat {
    pub ms: u16,
    pub lbads: u8,
    pub rp: B2,
    #[skip]
    __rsvd: B6,
}
