use std::{env, path::PathBuf, process::Command};

use bindgen_cfg::BindgenExt;
use pkg_config::Config;

fn main() {
    // Use the specified SPDK repo or default to the vendored submodule.
    let spdk_dir: PathBuf = env::var("SPDK_DIR")
        .ok()
        .unwrap_or_else(|| "vendor".to_string())
        .into();

    // Changes to the underlying SPDK library should not be occurring in this
    // project, so we're using a naive build caching method. Only build if the
    // build directory doesn't exist.
    let build_dir = spdk_dir.join("build");
    if !build_dir.join("include").exists() {
        Command::new("./configure")
            .current_dir(&spdk_dir)
            .output()
            .expect("Configure SPDK.");
        Command::new("make")
            .current_dir(&spdk_dir)
            .output()
            .expect("Make SPDK.");
    }

    // Create the rust bindings from the SPDK headers.
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindgen::builder()
        .clang_arg(format!("-I{}", build_dir.join("include").to_string_lossy()))
        .header("src/wrapper.h")
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .config("bindgen.yaml")
        .expect("bindgen config is present and well formed")
        .generate()
        .expect("Generate SPDK bindings.")
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Write SPDK bindings.");

    // Actually link to the SPDK libraries.
    env::set_var("PKG_CONFIG_PATH", build_dir.join("lib").join("pkgconfig"));
    for lib_name in ["spdk_env_dpdk", "spdk_util", "spdk_nvme"] {
        let lib = Config::new()
            .statik(true)
            .cargo_metadata(false)
            .probe(lib_name)
            .unwrap();
        for lib_dir in lib.link_paths {
            println!(
                "cargo:rustc-link-search=native={}",
                lib_dir.to_string_lossy()
            );
        }
        for dep in lib.libs {
            println!("cargo:rustc-link-lib=static:+whole-archive={}", dep);
        }
    }
    Config::new().statik(true).probe("spdk_syslibs").unwrap();
}
